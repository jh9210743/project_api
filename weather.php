<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>天氣預報</title>
    <script
    src="https://code.jquery.com/jquery-3.4.0.js"
    integrity="sha256-DYZMCC8HTC+QDr5QNaIcfR7VSPtcISykd+6eSmBW5qo="
    crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <script type="text/javascript" src="js/all.js"></script>
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
    <div class="jumbotron heroImage text-center">
        <div class="container">
            <h1 id="weather" class="display-4 text-light mt-5">天氣預報</h1>
            <p id="search" class="lead text-light">請再如下輸入框輸入您要查詢的<strong class="text-warning">城市名稱(英文)</strong></p>

            <form>
                <div class="from-group col-md-7 mx-auto">
                    <input id="city" class="form-control" type="text" name="city" placeholder="例如London、Paris、San Antonio...">
                    
                </div>
                   
            </form>
            <button id="findMyWeather" type="submit" name="submit" class="btn btn-warning btn-lg mt-3">查詢</button> 

            <div class="col-8 mx-auto mt-3">
                <div id ="success" class="alert alert-success">查詢成功</div>
                <div id ="fail" class="alert alert-danger">無法找到您查詢的城市</div>
                <div id ="noCity" class="alert alert-danger">請輸入城市名稱</div>
            </div>   
            
        </div>
    </div>



    </body>
</html>